#ifndef __VERSION_H__
#define __VERSION_H__

// ---- Update these
#define VERSION_MAJOR  1
#define VERSION_MINOR  0
#define VERSION_REV    0
#define VERSION_SUBREV 0

#define APP_NAME             "libDWARFpe"
#define APP_DESCRIPTION      "DWARF symbols parser, based on mgwhelp and libdwarf."
#define APP_PRODUCT          "libDWARFpe"
#define APP_COMPANY          "LGPL 2.1, David Anderson, SGI, J. Fonseca, ART Technology SAS"
#define APP_COPYRIGHT_DATE   "1995-2018"

// ---- Don't change these
#define VERSION_DOTTED      VERSION_MAJOR.VERSION_MINOR.VERSION_REV.VERSION_SUBREV
#define VERSION_COMMATTED   VERSION_MAJOR,VERSION_MINOR,VERSION_REV,VERSION_SUBREV
#define VERSION_DESCRIPTION APP_PRODUCT ": " APP_DESCRIPTION
#define VERSION_COPYRIGHT   "Copyright © " APP_COMPANY " " APP_COPYRIGHT_DATE

#define VERSION_STR0(V) #V
#define VERSION_STR(V) VERSION_STR0(V)

#endif
